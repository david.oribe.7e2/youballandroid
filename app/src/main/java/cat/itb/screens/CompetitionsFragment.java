package cat.itb.screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.R;
import cat.itb.manager.JfdataManager;
import cat.itb.model.player.Player;

public class CompetitionsFragment extends Fragment {

    @BindView(R.id.message)
    TextView message;
    private CompetitionsViewModel mViewModel;

    public static CompetitionsFragment newInstance() {
        return new CompetitionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.competitions_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CompetitionsViewModel.class);
        // TODO: Use the ViewModel
        JfdataManager jfdataManager = new JfdataManager("5438f56e3fa542708770faae16330c00");
        Player player=jfdataManager.getPlayer(44);
        message.setText(player.getName());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }
}
